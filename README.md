# AI Bartender (Kimmy)

Made by Ori Segal using Unity version 2020.3.40f1 (URP)

## Overview

AI Bartender or "Kimmy" is a test application for an automated bartender. The app is able to recognize voice and gives responses based on key words.

The application works through 3 main systems, which I will elaborate on further.

The application has 3 different states:

- WaitingState

- GreetingState

- CocktailState

Playing the app will initiate all the systems and put Kimmy in the "WaitingState." This state will work repetitively and will act only with a sentence including the word "Hello".

*Note: the following states do not work repetitively.*

After recognizing the word "hello," Kimmy will switch to the "GreetingState," then react only for the words "Recommend" and "Exit"

Finally, the CocktailState will react to the word "Exit".

However, in any state of your choosing to exit "Kimmy" will automatically cause to switch back to the "WaitingState" and start listening repetitively.

Logs will be thrown to the console when Kimmy is listening and speaking, as well as the sentences that she receives.  

## Project systems 

### SpeechSystem

This system is responsible for the voice recognition and  speech (text to speech, speech to text).

This system contains 3 classes;

- Listen: responsible for the speech to text services and its configurations.

- Speak: responsible for the text to speech services and its configurations.

- ConversationManager: responsible for the initiating the two other classes and to expose the chosen abilities to other scripts.

  

### KimmySystem (State Machine)

This system is able to control Kimmy and switch between her states which this contains the following;

- KimmyManager: to control Kimmy.

- KimmyStateMachine: the abstract class for the state machine.

- KeyWords: class that holds all the key words value. This class is serializable for the future purposes of loading it dynamically from the server and have the ability to change key words that Kimmy reacts for on the flight.

Including the previously mentioned states.

  

### ServerCommunication

This system controls the server communication with two classes: templates and the ServerCommunicator.
  

## Note

- The project materials were converted to URP baked lit to work with the URP. Even though the project wasn't optimized, the decision to use baked lit materials came from the thoughts of baking light in the future.

  

- Animations are only to switch examples.

Help sources:
[About the Speech SDK - Speech service - Azure Cognitive Services | Microsoft Learn](https://learn.microsoft.com/en-us/azure/cognitive-services/speech-service/speech-sdk)

https://docs.unity3d.com/Manual/index.html

https://www.youtube.com/
