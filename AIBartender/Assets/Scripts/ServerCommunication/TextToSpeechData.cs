using Firebase.Firestore;

namespace ServerCommunication
{
    [FirestoreData]
    public struct TextToSpeechData
    {
        [FirestoreProperty]
        public string sentence { get; set; }
    }
}

