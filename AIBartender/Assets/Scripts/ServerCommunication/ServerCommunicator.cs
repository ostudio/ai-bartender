using Firebase.Firestore;
using Firebase.Extensions;

namespace ServerCommunication
{
    public class ServerCommunicator
    {
        FirebaseFirestore FirebaseFirestore;

        public void SendData(string sentence, string session, string timeStemp)
        {
            FirebaseFirestore = FirebaseFirestore.DefaultInstance;
            TextToSpeechData textToSpeechData = new TextToSpeechData();

            textToSpeechData.sentence = sentence;

            string doc = session + " " + timeStemp;

            DocumentReference document = FirebaseFirestore.Collection("cecilia_conversation").Document(doc);
            document.SetAsync(textToSpeechData).ContinueWithOnMainThread(task => { });
        }
    }
}

