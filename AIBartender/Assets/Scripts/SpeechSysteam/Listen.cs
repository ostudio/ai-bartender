using System.Threading.Tasks;
using Microsoft.CognitiveServices.Speech;
using Microsoft.CognitiveServices.Speech.Audio;

namespace SpeechSysteam
{
    public class Listen 
    {
        // This class responsible to initialize and configure the speech to text

        SpeechRecognizer recognizer;

        public Listen(string key, string region)
        {
            InitializeSpeechToText(key,region);
        }

        void InitializeSpeechToText(string key, string region)
        {
            var speechConfig = SpeechConfig.FromSubscription(key, region);
            var audioConfig = AudioConfig.FromDefaultMicrophoneInput();
            recognizer = new SpeechRecognizer(speechConfig, audioConfig);
        }
        public async Task<string> StartListen()
        {
            var result = await recognizer.RecognizeOnceAsync();
            string receivedSentence = result.Text;

            return receivedSentence;
        }

    }
}

