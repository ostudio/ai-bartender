using System.Threading.Tasks;
using UnityEngine;

namespace SpeechSysteam
{
    public class ConversationManager : MonoBehaviour
    {
        // This is the class that im exposeing to use 
        // the speech SDK services

        static protected Listen listen;
        static protected Speak speak;

        string listenkey = "2352ed5e3ff147399bad73ea0236b5bf";
        string speakKey = "9ae64f437dfe47f281254b621b4ccfc0";
        string region = "westeurope";

        void Awake()
        {
            listen = new Listen(listenkey, region);
            speak = new Speak(speakKey, region);

            string defualtMic = Microphone.devices[0];

            Debug.Log("Your default microphone is " + defualtMic); // Just to check if there is more then one device
        }

        public static async Task<string> GetReceivedSentence() // The expose method to recognize text
        {
            try
            {
                Debug.Log("Listening...");

                string receivedSentence = await listen.StartListen();

                Debug.Log("Sentence received: " + receivedSentence);
                return receivedSentence.ToLower();
            }
            catch
            {
                return "";
            }

        }

        public static async Task Speak(string whatToSay) // The expose method to use text to speak
        {
            try
            {
                Debug.Log("Speaking...");
                await speak.StartSpeak(whatToSay);
            }
            catch
            {
                return;
            }
        }
    }
}

