using System.Threading.Tasks;
using Microsoft.CognitiveServices.Speech;

namespace SpeechSysteam
{
    public class Speak
    {
        // This class responsible to initialize and configure the text to speech

        SpeechSynthesizer synthesizer;

        public Speak(string key, string region)
        {
            InitializeTextToSpeech(key, region);
        }

        void InitializeTextToSpeech(string key, string region)
        {
            var speechConfig = SpeechConfig.FromSubscription(key, region);
            speechConfig.SpeechSynthesisLanguage = "en-US";
            speechConfig.SpeechSynthesisVoiceName = "en-US-JennyNeural";

            synthesizer = new SpeechSynthesizer(speechConfig);
        }

        public async Task StartSpeak(string say)
        {
            await synthesizer.SpeakTextAsync(say);
        }
    }
}

