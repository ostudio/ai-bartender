namespace Kimmy
{
    public abstract class KimmyStateMachine
    {
        // The state machine class that all states should inspire from

        public KimmyManager _kimmyManager;
        public KeyWords keyWords;
        public KimmyStateMachine(KimmyManager kimmyManager)
        {
            _kimmyManager = kimmyManager;
            keyWords = new KeyWords();
        }

        public abstract void EnterState();

        public abstract void Listening();

        public abstract void Speaking(string say);


        public virtual bool IsExit(string sentenceToCheck) // Check if user want to end conversation
        {
            if (sentenceToCheck.Contains(keyWords.endConversationStateKeyWords[0]))
            {
                return true;
            }

            return false;
        }
    }
}

