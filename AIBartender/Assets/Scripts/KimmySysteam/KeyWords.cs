using System;

namespace Kimmy
{
    [Serializable]
    public class KeyWords
    {
        public string[] waitingStateKeyWords = { "hello" };
        public string[] greetingStateKeyWords = { "recommend" };
        public string[] cocktailStateKeyWords;
        public string[] endConversationStateKeyWords = { "exit" };
    }
}

