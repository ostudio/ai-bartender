using SpeechSysteam;

namespace Kimmy.State
{
    public class GreetingState : KimmyStateMachine
    {
        public GreetingState(KimmyManager kimmyManager) : base(kimmyManager) { }

        public override void EnterState()
        {
            _kimmyManager.KimmySpeaking("Hello, welcome to the test application. What can i get you to drink?");
            _kimmyManager.PlayKimmyAnim("Greeting");
        }

        public override async void Listening()
        {
            string receivedSentence = await ConversationManager.GetReceivedSentence();

            if (base.IsExit(receivedSentence))
            {
                _kimmyManager.KimmySpeaking("Thank you for chatting with me. Have a nice day");
                _kimmyManager.SwitchState(_kimmyManager.waitingState);
            }
            else if (receivedSentence.Contains(keyWords.greetingStateKeyWords[0]))
            {
                _kimmyManager.SwitchState(_kimmyManager.coocktailState);
            }
        }

        public override async void Speaking(string say)
        {
            await ConversationManager.Speak(say);
            _kimmyManager.KimmyListening();
        }
    }
}

