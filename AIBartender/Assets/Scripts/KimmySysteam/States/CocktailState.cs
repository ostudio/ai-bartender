using SpeechSysteam;

namespace Kimmy.State
{
    public class CocktailState : KimmyStateMachine
    {
        public CocktailState(KimmyManager kimmyManager) : base(kimmyManager) { }

        public override void EnterState()
        {
            _kimmyManager.KimmySpeaking("It depends, do you prefer your cocktail sweet, fruity, or classic?");
            _kimmyManager.PlayKimmyAnim("OfferCocktail");
        }

        public override async void Listening()
        {
            string receivedSentence = await ConversationManager.GetReceivedSentence();

            if (base.IsExit(receivedSentence))
            {
                _kimmyManager.KimmySpeaking("Thank you for chatting with me. Have a nice day");
                _kimmyManager.SwitchState(_kimmyManager.waitingState);
            }
        }

        public override async void Speaking(string say)
        {
            await ConversationManager.Speak(say);
            _kimmyManager.KimmyListening();
        }
    }
}

