using UnityEngine;
using SpeechSysteam;


namespace Kimmy.State
{
    public class WaitingState : KimmyStateMachine
    {
        public WaitingState(KimmyManager kimmyManager) : base(kimmyManager) { }

        public override void EnterState() { }

        public override async void Listening()
        {
            string receivedSentence = await ConversationManager.GetReceivedSentence();

            if(receivedSentence.Contains(keyWords.waitingStateKeyWords[0]))
            {
                _kimmyManager.SwitchState(_kimmyManager.greetingState);
            }
            else
            {
                if (!Application.isPlaying)
                    return;
                
                _kimmyManager.KimmyListening();
            }
        }

        public override async void Speaking(string say)
        {
           // Optinal Say hello while wating 
        }
    }
}

