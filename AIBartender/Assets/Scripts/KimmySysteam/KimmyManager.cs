using System;
using UnityEngine;
using Kimmy.State;
using ServerCommunication;
using UnityEngine.Analytics;

namespace Kimmy
{
    public class KimmyManager : MonoBehaviour
    {
        KimmyStateMachine currentState;
        ServerCommunicator serverCommunicator;

        public WaitingState waitingState;
        public GreetingState greetingState;
        public CocktailState coocktailState;

        Animator animator;
        string sessionId;

        void Awake()
        {
            serverCommunicator = new ServerCommunicator();
            sessionId = AnalyticsSessionInfo.sessionId.ToString();

            waitingState = new WaitingState(this);
            greetingState = new GreetingState(this);
            coocktailState = new CocktailState(this);
        }

        void Start()
        {
            currentState = waitingState;
            currentState.Listening();

            animator = GetComponent<Animator>();
        }

        public void KimmyListening()
        {
            currentState.Listening();
        }

        public void KimmySpeaking(string whatKimmySay)
        {
            currentState.Speaking(whatKimmySay);
            SendData(whatKimmySay);
        }

        public void SwitchState(KimmyStateMachine newState)
        {
            currentState = newState;
            currentState.EnterState();
        }

        public void PlayKimmyAnim(string whichAnimToPlay)
        {
            animator.Play(whichAnimToPlay);
        }

        void SendData(string whatToSend)
        {
            string timeStemp = DateTime.Now.ToString();

            serverCommunicator.SendData(whatToSend, sessionId, timeStemp);
        }
    }
}

